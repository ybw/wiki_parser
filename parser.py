#!./venv/bin/python

import re
import argparse
import os


regexp = (
    (re.compile(r'#Type:\s.*'), ''),
    (re.compile(r'^\s+'), ''),
    (re.compile(r'\s+$'), '')
)

def write_artcile(title, text, depth):
    for exp, repl in regexp:
        text = exp.sub(repl, text)

    title = title.decode('utf8')
    filename = title.replace('/', ' ')
    filename = filename.lower()
    filename = re.sub(r'\W+', ' ', filename)
    filename = filename[:100]
    filename = filename + '.txt'

    folder_tree = title[:depth]
    folder_tree = folder_tree.lower()
    folder_tree = folder_tree.replace('/', '_')
    folder_tree = re.sub(r'\W+', '_', folder_tree)

    if len(title) < depth:
        folder_tree = folder_tree.ljust(depth)
    folder_tree = folder_tree.replace(' ', '_')

    destination_directory = os.path.join(
        'article', *list(folder_tree))
    if not os.path.exists(destination_directory):
        os.makedirs(destination_directory)

    # Check file exists, add prefix if exists
    destination = '/'.join([destination_directory, filename])
    while os.path.isfile(destination):
        filename = '_'+filename
        destination = '/'.join([destination_directory, filename])
    with open(destination, 'w') as f:
        f.write(text)
    try:
        print u'Article %s writen' % title
    except UnicodeError:
        pass

def run(source, depth):
    title_regexp = re.compile(r'#Article:\s(.*)')
    see_also_regexp = re.compile(r'^See also$')
    spaces = re.compile(r'^\s+')

    title = text = ''
    see_also = False

    with open(source, 'r') as source_fd:
        for line in source_fd:
            title_match = re.match(title_regexp, line)
            if title_match:
                if title and text:
                    write_artcile(title, text, depth)
                text = ''
                title = title_match.group(1)
                continue
            if re.match(see_also_regexp, line):
                see_also = True
                continue
            if see_also and re.match(spaces, line):
                continue
            see_also = False
            text += line

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--source', help=u'wikipedia file', required=True)
    parser.add_argument('--depth', help=u'folder depth', default=3, type=int)
    params = parser.parse_args()
    run(params.source, params.depth)
